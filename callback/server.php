<?php

// Commercegate callback server
$array=split("#",$_REQUEST['message']);
$cb= new stdClass;
$cb->transactiontype=$array[0];
$cb->transactionid=$array[1];
$cb->transactionreferenceid=$array[2];
$cb->offername=$array[3];
$cb->offerid=$array[4];
$cb->amount=$array[5];
$cb->currency=$array[6];
$cb->userid=$array[7];
$cb->userpw=$array[8];
$cb->email=$array[9];
$cb->ip=$array[10];
$cb->countryiso=$array[11];
$cb->cardholder=$array[12];
$cb->customerid=$array[13];
$cb->websiteid=$array[14];
$cb->op1=$array[15];
$cb->op2=$array[16];
$cb->op3=$array[17];
$cb->nmu=$array[18];
$cb->date= date( 'Y-m-d H:i:s'); 

// log the call
$file="callback.log";
if (!$fh = fopen($file, 'a')) {
	echo ("cannot open log file ".$file);
} else {
	date_default_timezone_set('CET');
	$stringData = date('l jS \of F Y h:i:s A'). " -> http://".$_SERVER["SERVER_NAME"]."/sites/all/modules/commercegate/callback/server.php?messagetype=".$_REQUEST['messagetype']."&message=".urlencode($_REQUEST['message'])."\n";
	fwrite($fh, $stringData);
	fwrite($fh, "On date ".$cb->date." user ".$cb->userid." (".$cb->email.") did a ".$cb->transactiontype." transaction\n";);
	fclose($fh);
}

/**
$db = new database(); // our database object 

//check transaction type
switch ($cb->transactiontype) {
	case "SALE": // insert the user.
		if (!$db->insertUser($cb)) {
			echo "user ".$cb->userid." could not be inserted to the database<br>";
			echo "Database error: ".$db->stderr();
		}
		break;
	case "REBILL": // Rebill user
		if (!$db->rebillUser($cb)) {
			echo "user ".$cb->userid." could not be rebilled<br>";
			echo "Database error: ".$db->stderr();
		}
		break;
	case "UPSELL": // Upsell user
		if (!$db->upsellUser($cb)) {
			echo "user ".$cb->userid." could not be upselled<br>";
			echo "Database error: ".$db->stderr();
		}
		break;
	case "REFUND": // refund user
		if (!$db->refundUser($cb)) {
			echo "user ".$cb->userid." could not be marked for refund<br>";
			echo "Database error: ".$db->stderr()."<br>";
		}
		break;
	case "CHARGEBACK": // chargeback  user
		if (!$db->chargebackUser($cb)) {
			echo "user ".$cb->userid." could not be marked for chargeback<br>";
			echo "Database error: ".$db->stderr()."<br>";
		}
		break;
	default:
		echo "unsupported callback type";
		break;
}
**/

echo "SUCCESS";
?>