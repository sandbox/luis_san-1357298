
Configuraci�n del m�dulo

ATENCION
NO instalar este m�dulo cuando ya se encuentra instalado y activo el m�dulo de Commerce Gate (commercegate)
Esto causa un error que puede dejar su sitio sin funcionar, ya que en ambos m�dulos existe un m�todo llamado
"callback_server" y genera el siguiente error: "Cannot redeclare callback_server() in ... on line ..."
Si este error ocurre y no puede volver a desactivar el m�dulo por la interfaz, intente uno de estos 2 puntos:
- Desactive el m�dulo por la base de datos
- En el archivo "commercegate.module" cambie el nombre a la function "callback_server", recargue
la pantalla y dir�jase a modules para desactivar el m�dulo "commercegate", ahora vuelva el nombre de la funci�n
a su estado anterior.
